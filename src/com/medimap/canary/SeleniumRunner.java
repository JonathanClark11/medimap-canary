package com.medimap.canary;

import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by jonclark on 2016-04-21.
 */
public class SeleniumRunner {

    public static void main(String[] args) {
        // declaration and instantiation of objects/variables
        WebDriver driver = new FirefoxDriver();

        testBasicLoad(driver);

        driver.close();
        // exit the program explicitly
        System.exit(0);
    }

    private static void testBasicLoad(WebDriver driver) {
        String baseUrl = "http://medimap.ca";
        String expectedTitle = "Medimap | Walk-in Clinic Wait Times";
        String actualTitle = "";

        // launch Firefox and direct it to the Base URL
        driver.get(baseUrl);

        // get the actual value of the title
        actualTitle = driver.getTitle();

        /*
         * compare the actual title of the page witht the expected one and print
         * the result as "Passed" or "Failed"
         */
        if (actualTitle.contentEquals(expectedTitle)){
            System.out.println("Test Passed!");
        } else {
            System.out.println("Test Failed");
        }

        WebElement location = driver.findElement(By.cssSelector("input#address"));
        location.sendKeys("Victoria");

        driver.findElement(By.cssSelector("button[type=submit]")).click();

        try {
            //wait till it loads and assert clinics loaded
            WebDriverWait waitDriver = new WebDriverWait(driver, 6);
            waitDriver.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("li[data-id=\"633\"]")));
        } catch (TimeoutException te) {
            System.out.println("Took too long to load");
        }

        System.out.println("Found Yates");

        testBasicLoad(driver);
    }
}
